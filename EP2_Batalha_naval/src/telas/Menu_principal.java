package telas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import canvas.CanvasJogo;
import canvas.InterfacePrincipal;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import javax.swing.UIManager;
import java.awt.Color;

public class Menu_principal extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Menu_principal frame = new Menu_principal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	
	public Menu_principal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Batalha Naval 2.0");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("American Typewriter", Font.PLAIN, 21));
		lblNewLabel.setBounds(143, 35, 198, 71);
		contentPane.add(lblNewLabel);
		
		JButton btnNewButton = new JButton("Novo jogo");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CanvasJogo canvas = new CanvasJogo();
				 canvas.setVisible(true);
				 InterfacePrincipal frame = new InterfacePrincipal();
					frame.setVisible(true);
			}
		});
		btnNewButton.setBounds(43, 165, 117, 29);
		contentPane.add(btnNewButton);
		
		JButton btnInstrues = new JButton("Instruções");
		btnInstrues.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			 Guide guide=new Guide();
			 guide.setVisible(true);
			}
		});
		btnInstrues.setBounds(298, 165, 117, 29);
		contentPane.add(btnInstrues);
		
		JLabel label = new JLabel("Menu");
		label.setFont(new Font("Copperplate", Font.PLAIN, 20));
		label.setIcon(new ImageIcon("/Users/wancley/Downloads/Batalha.jpg"));
		label.setBounds(0, 0, 450, 278);
		contentPane.add(label);
		
	}
}
